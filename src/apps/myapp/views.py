from django.views.generic.detail import BaseDetailView
from django.views.generic.base import TemplateResponseMixin
from .models import Page#, ListNode


class IndexView(TemplateResponseMixin, BaseDetailView):
    model = Page
    template_name = 'myapp/index.html'
    context_object_name = 'page'

    def get_object(self, queryset=None):
        return self.model.get_solo()

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
        # context.update({
        #     'nodes': ListNode.objects.all(),
        # })
        # return context
