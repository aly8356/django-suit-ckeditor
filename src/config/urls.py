from django.contrib import admin
from django.conf.urls import include, url
from django.views.i18n import JavaScriptCatalog
from django.conf import settings


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(), name='jsi18n'),
    url('^myapp/', include('myapp.urls', namespace='myapp')),
]

if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns = (
            static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +
            static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) +
            urlpatterns
    )
