from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import resolve_url
from solo.models import SingletonModel


class Page(SingletonModel):
    header = models.CharField(_('header'), max_length=255)
    description = models.TextField(_('description'), blank=True)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    def __str__(self):
        return self.header

    def get_absolute_url(self):
        return resolve_url('myapp:index')

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('Settings')


class ListNode(models.Model):
    page = models.ForeignKey(Page, verbose_name=_('page'), related_name='+')
    theme = models.CharField(_('theme'), max_length=255)
    text = models.TextField(_('text'), blank=True)

    created_at = models.DateTimeField(_('created at'), default=now, editable=False)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        verbose_name = _('ListNode')
        verbose_name_plural = _('ListNodes')

    def __str__(self):
        return self.theme
