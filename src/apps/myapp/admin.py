from django import forms
from django.contrib import admin
from solo.admin import SingletonModelAdmin
from ckeditor.widgets import CKEditorWidget
from .models import Page, ListNode


class ListNodeForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = ListNode
        fields = '__all__'


class ListNodeInlineAdmin(admin.StackedInline):
    model = ListNode
    form = ListNodeForm
    extra = 0


@admin.register(Page)
class PageAdmin(SingletonModelAdmin):
    inlines = (ListNodeInlineAdmin,)
    fieldsets = (
        (None, {
            'fields': (
                'header', 'description',
            ),
        }),
        # (None, {
        #     'fields': (
        #         'text',
        #     ),
        # }),
    )
