
`python3 --version`  
`python3 -m venv venv`  
`source venv/bin/activate`  
`pip install -r requirements.txt`  
`python3 src/manage.py migrate`  
`python3 src/manage.py createsuperuser`  
`python3 src/manage.py runserver`
